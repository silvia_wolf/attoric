-- MySQL dump 10.13  Distrib 8.0.27, for Linux (x86_64)
--
-- Host: localhost    Database: analisi_programmazione
-- ------------------------------------------------------
-- Server version	8.0.27-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `actor`
--

DROP TABLE IF EXISTS `actor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `actor` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome` varchar(100) DEFAULT NULL,
  `cognome` varchar(100) DEFAULT NULL,
  `anno` int DEFAULT NULL,
  `cod_fisc` varchar(16) DEFAULT NULL,
  `ruolo` varchar(100) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `actor`
--

LOCK TABLES `actor` WRITE;
/*!40000 ALTER TABLE `actor` DISABLE KEYS */;
/*!40000 ALTER TABLE `actor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `banca_dati`
--

DROP TABLE IF EXISTS `banca_dati`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `banca_dati` (
  `id` int NOT NULL AUTO_INCREMENT,
  `titolo_italiano` varchar(100) DEFAULT NULL,
  `titolo_originale` varchar(100) DEFAULT NULL,
  `numero_stagione` varchar(100) DEFAULT NULL,
  `titolo_puntata` varchar(100) DEFAULT NULL,
  `numero_puntata` varchar(100) DEFAULT NULL,
  `artista_rasi_primari` varchar(100) DEFAULT NULL,
  `artista_rasi_comprimari` varchar(100) DEFAULT NULL,
  `artista_rasi_doppiatori_primari` varchar(100) DEFAULT NULL,
  `artista_rasi_doppiatori_comprimari` varchar(100) DEFAULT NULL,
  `regia` varchar(100) DEFAULT NULL,
  `anno_pubblicazione` varchar(4) DEFAULT NULL,
  `anno_produzione` varchar(100) DEFAULT NULL,
  `tipologia_opera` varchar(100) DEFAULT NULL,
  `tipo_trasmissione` varchar(100) DEFAULT NULL,
  `paese_produzione` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banca_dati`
--

LOCK TABLES `banca_dati` WRITE;
/*!40000 ALTER TABLE `banca_dati` DISABLE KEYS */;
/*!40000 ALTER TABLE `banca_dati` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `emittente`
--

DROP TABLE IF EXISTS `emittente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `emittente` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome_emittente` varchar(100) DEFAULT NULL,
  `id_gruppo` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `emittente`
--

LOCK TABLES `emittente` WRITE;
/*!40000 ALTER TABLE `emittente` DISABLE KEYS */;
/*!40000 ALTER TABLE `emittente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `movie`
--

DROP TABLE IF EXISTS `movie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `movie` (
  `id` int NOT NULL AUTO_INCREMENT,
  `titoloOriginale` varchar(100) DEFAULT NULL,
  `titoloItaliano` varchar(100) DEFAULT NULL,
  `annoProduzione` int DEFAULT NULL,
  `regiaId` int DEFAULT NULL,
  `durata` int DEFAULT NULL,
  `paeseProduzione` varchar(4) DEFAULT NULL,
  `distributore` varchar(50) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `noDoubleMovies` (`titoloOriginale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `movie`
--

LOCK TABLES `movie` WRITE;
/*!40000 ALTER TABLE `movie` DISABLE KEYS */;
/*!40000 ALTER TABLE `movie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `parsati`
--

DROP TABLE IF EXISTS `parsati`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `parsati` (
  `id` int NOT NULL AUTO_INCREMENT,
  `nome_file` varchar(100) DEFAULT NULL,
  `numero_righe` int DEFAULT NULL,
  `data_parsing` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `parsati`
--

LOCK TABLES `parsati` WRITE;
/*!40000 ALTER TABLE `parsati` DISABLE KEYS */;
/*!40000 ALTER TABLE `parsati` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trasmissione`
--

DROP TABLE IF EXISTS `trasmissione`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `trasmissione` (
  `id` int NOT NULL AUTO_INCREMENT,
  `id_canale` int DEFAULT NULL,
  `data` varchar(10) DEFAULT NULL,
  `ora_inizio` varchar(8) DEFAULT NULL,
  `ora_fine` varchar(8) DEFAULT NULL,
  `titolo_opera` varchar(100) DEFAULT NULL,
  `titolo_opera_originale` varchar(100) DEFAULT NULL,
  `titolo_puntata` varchar(100) DEFAULT NULL,
  `titolo_puntata_originale` varchar(100) DEFAULT NULL,
  `numero_puntata` varchar(100) DEFAULT NULL,
  `numero_stagione` varchar(100) DEFAULT NULL,
  `regia` varchar(1000) DEFAULT NULL,
  `note` varchar(1000) DEFAULT NULL,
  `id_file_provenienza` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trasmissione`
--

LOCK TABLES `trasmissione` WRITE;
/*!40000 ALTER TABLE `trasmissione` DISABLE KEYS */;
/*!40000 ALTER TABLE `trasmissione` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-12-16 13:31:14
